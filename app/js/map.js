document.addEventListener('DOMContentLoaded',function(){
 ymaps.ready(init);
    var myMap,
        myPlacemark,
        myPlacemark1,
        myPlacemark2,
        myPlacemark3,
        myCollection,
        coords = [
    [55.74221295, 37.62927680],
    [55.74158477, 37.62865089],
    [55.74157145, 37.62835373],
    [55.74216376, 37.62864307]
                     ];

    function init(){     
        myMap = new ymaps.Map("map", {
            center: [55.74188790, 37.62898501],
            zoom: 18
        });
        
        myMap.controls.remove('searchControl');
        myMap.behaviors
            // Отключаем некоторые включенные по умолчанию поведения:
            //  - drag - перемещение карты при нажатой левой кнопки мыши;
            //  - rightMouseButtonMagnifier - увеличение области, выделенной
            //    правой кнопкой мыши.
        .disable(['drag', 'rightMouseButtonMagnifier','scrollZoom']);

         myCollection = new ymaps.GeoObjectCollection({}, {
            iconLayout: 'default#image',
            iconImageHref: 'img/map/map-marker.png',
            iconImageSize: [46, 57],
            iconImageOffset: [-20, -50]
        });
         myPlacemark1 = new ymaps.Placemark([55.74158477, 37.62865089], 
         { 
             hintContent: 'Москва!', 
             balloonContent: 'Столица России' 
         });
         myPlacemark2 = new ymaps.Placemark([55.74157145, 37.62835373], 
         { 
             hintContent: 'Москва!', 
             balloonContent: 'Столица России' 
         });
         myPlacemark3 = new ymaps.Placemark([55.74158175, 37.62837033], 
         { 
             hintContent: 'Москва!', 
             balloonContent: 'Столица России' 
         });
         for (var i = 0; i < coords.length; i++) {
            myCollection.add(new ymaps.Placemark(coords[i]));
        }
        myMap.geoObjects.add(myCollection);
         
         
    }
      
          
    
       
    
});